import java.io.DataInputStream;

/**
 * Created by pacio on 08.03.2017.
 */
public class ZadanieOdbieranie implements Zadanie{
    String nazwa="Odbieranie";
    @Override
    public String nazwa() {
        return nazwa;
    }

    @Override
    public void wykonaj() {
        DataInputStream dis=(DataInputStream) Tools.znajdzObiekt("dis");
        String napiso="";
        String napis= Tools.czytaj(dis);

        char[] tab=new char[napis.length()];
        for(int n=0;n< napis.length();n++){
            tab[n]=napis.charAt(n);
        }
        int polow=napis.length()/2;

        for(int n=polow;n<napis.length();n++){
            for(int m=n;m<(napis.length());m++){
                if(tab[n]>tab[m]){
                    char tmp=tab[n];
                    char tmp2=tab[m];
                    tab[n]=tmp2;
                    tab[m]=tmp;
                }
            }

        }

        for(int n=0;n<napis.length();n++){
            napiso+=tab[n];
        }
        System.out.println(napiso);
    }
}
