import java.io.BufferedReader;
import java.io.DataInputStream;

/**
 * Created by pacio on 15.03.2017.
 */
public class ZadanieCzytanie implements Zadanie{
    String nazwa="Czytanie";
    @Override
    public String nazwa() {
        return nazwa;
    }

    @Override
    public void wykonaj() {
       // DataInputStream dis= (DataInputStream) Tools.znajdzObiekt("dis");
        BufferedReader br= (BufferedReader) Tools.znajdzObiekt("br");
        String napis=Tools.czytajAsync(br);

        System.out.println(">> klient pisze: "+napis);

    }
}
