import java.io.BufferedReader;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * Created by pacio on 29.03.2017.
 */
public class ZadanieKlientUtrzymany implements Zadanie{
    public String nazwa="Utrzymywanie polaczenia Klient";
    @Override
    public String nazwa() {
        return nazwa;
    }
    @Override
    public void wykonaj() {
        System.out.println("Tu sie dzieje magia");
        BufferedReader br= (BufferedReader) Tools.znajdzObiekt("br");
        ObjectOutputStream oos= (ObjectOutputStream) Tools.znajdzObiekt("oos");
        boolean work=true;
        boolean poczatek=true;
        int odp;
        while(true){
           // Sasiad s=(Sasiad) Tools.znajdzObiekt("sasiad");
            //System.err.println("Zyje" +s.getNazwa());
            try {
                odp=br.read();
                if(odp==0){
                    System.out.println("odp 0");
                    oos.writeObject(new Sasiad(Info.Ip,Info.Port,Info.Imie));
                }else if(odp==2){
                    Tools.dodajZadanieK(new ZadanieOdbieranieBrodcast());
                    System.out.println("odp 2");
                    Tools.wykonajZadaniaK();
                }else if(odp == 1){
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
