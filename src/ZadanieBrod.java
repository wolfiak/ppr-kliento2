import java.io.ObjectOutputStream;
import java.util.Scanner;

/**
 * Created by pacio on 05.04.2017.
 */
public class ZadanieBrod implements  Zadanie {
    Sasiad s;
    Stan stan=Stan.Klient;
    private boolean dzialanie=true;
    @Override
    public String nazwa() {
        return "Zadanie brodcast wysylam";
    }

    @Override
    public void wykonaj() {
        System.out.println("Klient>> Wysylam obiekt");
        Tools.wyslijObiekt(new Sasiad(Info.Ip,Info.Port,Info.Imie),Stan.Klient);
        System.out.println("Klient>> Obiekt wysalny");
        System.out.println("Klient>> Czekanie na obiekt od serwera");
       s= (Sasiad) Tools.czytajObiektO(Stan.Klient);
        System.out.println("Klient>> Obiekt otrzymany");
        System.out.println("Klient>> Polaczono z "+s.getNazwa());
        Scanner skaner=new Scanner(System.in);
        while(dzialanie){
            System.out.print("Do kogo napisac wiadmosci: ");
            String doKogo=skaner.next();
            System.out.println("Wprowadz wiadomosci");
            String wiadomos=skaner.next();
            System.out.println("Wiadomosc rozgloszeniowa: (t/f):");
            String t=skaner.next();
            boolean odp;
            if(t.equals("t")){
                odp=true;
            }else{
                odp=false;
            }
            System.out.println("Klient>> Przed wyslaniem wiadomosci");
            Tools.wyslijObiekt(new Brodcast(Info.Imie,doKogo,wiadomos,odp),Stan.Klient);
            System.out.println("Klient>> Po wyslaniu");
        }
    }
}
