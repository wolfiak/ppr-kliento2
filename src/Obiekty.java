/**
 * Created by pacio on 08.03.2017.
 */
public class Obiekty {
    private String klucz;
    private Object wartosc;
    private boolean async;
    private Stan stan;
    private long id;
    public Obiekty(String klucz,Object wartosc){
        this.klucz=klucz;
        this.wartosc=wartosc;
        async=false;
    }
    public Obiekty(String klucz,long id,Object wartosc){
        this.klucz=klucz;
        this.id=id;
        this.wartosc=wartosc;
        async=true;
    }
    public Obiekty(String klucz,Object wartosc,Stan stan){
        this.klucz=klucz;
        this.wartosc=wartosc;
        this.stan=stan;
        async=false;
    }
    public String getKlucz(){
        return klucz;
    }
    public Object getWartosc(){
        return wartosc;
    }
    public long getId(){
        return id;
    }
    public boolean getAsync(){
        return async;
    }

    public Stan getStan() {
        return stan;
    }

    public void setStan(Stan stan) {
        this.stan = stan;
    }
}
