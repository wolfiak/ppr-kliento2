import java.util.Scanner;

/**
 * Created by pacio on 29.03.2017.
 */
public class Konsola {
    static String wiad=null;
    public static void start(){
        Scanner skaner=new Scanner(System.in);
        boolean dzialnie=true;
        System.out.println("Konsola aktywna");

        Runnable r=new Runnable() {
            @Override
            public void run() {
                while(dzialnie){
                    if(Tools.check) {
                        wiad = skaner.next();
                    }
                    switch(wiad){
                        case "help":{
                            System.out.println("Konsola jest aktywna");
                            System.out.println("---------------------------------------------------------------");
                            System.out.println("Komendy:");
                            System.out.println("wiad - Utworz nowa wiadomosc do pozostalych wezlow");


                            System.out.println("---------------------------------------------------------------");
                            break;
                        }
                        case "wiad":{
                            System.out.println("Poszlo zadanie");
                            //ZadanieUtrzymywaniePoalczenia.odp=2;
                            //Tools.dodajZadnanie(new ZadanieGenerowanieWiadomosci());
                            GenerowanieWiadomosci.uruchomSluchanie();
                            GenerowanieWiadomosci.generuj();
                            System.out.println(">>>> ROLA KONSOLI ZAKONCZONA");
                            Tools.check=false;
                            break;
                        }
                    }
                    wiad="";
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        };

        Thread t=new Thread(r);
        t.start();
    }
}
