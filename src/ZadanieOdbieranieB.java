import javax.tools.Tool;

/**
 * Created by pacio on 05.04.2017.
 */
public class ZadanieOdbieranieB implements  Zadanie {
    Sasiad s;
    @Override
    public String nazwa() {
        return "Zadanmie odbieranie brodcasr";
    }

    @Override
    public void wykonaj() {
        System.out.println("Serewr>> Oczekiwanie na obietk");
        s= (Sasiad) Tools.czytajObiektO(Stan.Serwer);
        System.out.println("Serewr>> Obiekt otrzymany");
        System.out.println("Serewr>> Polaczono z "+s.getNazwa());
        System.out.println("Serewr>> Wysylanie obiektu do "+s.getNazwa());
        Tools.wyslijObiekt(new Sasiad(Info.Ip,Info.Port,Info.Imie),Stan.Serwer);
        System.out.println("Serewr>> Obiekt Wyslany");
        System.out.println("Serewr>> Przed odberaniem obiektu");
        Brodcast b= (Brodcast) Tools.czytajObiektO(Stan.Serwer);
        if(b.isRozgloszeniowy()){
            System.out.println("Wiadomosic od "+b.getOd());
            if(b.getDokogo().equals(Info.Imie)){
                System.out.println("Wiadomosc do ciebie: "+b.getWiadomosc());
            }else{
                System.out.println("Wiadomosc jest do "+b.getDokogo()+" przekazuje ja dalej sasiadowi");
                ZadanieTranzyt t=new ZadanieTranzyt();
                t.obiekt(b);
                MenadzerK mk=new MenadzerK(t);
                Klient k=new Klient("localhost",3003,mk);
                k.POLACZ();
            }
        }else{
            System.out.println("Wiadomosic od "+b.getOd()+ "zostala odrzucona");
        }
    }
}
