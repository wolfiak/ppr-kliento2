import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by pacio on 08.03.2017.
 */
public class Serwer {
    protected static int port;
    protected static Menadzer m;
    public Serwer(int port, Menadzer m){
       this.port=port;
       this.m=m;
    }
    public  void START(){

        try {
            ServerSocket s=new ServerSocket(port);
            System.out.println("SERWER>> Serwer ruszył na porcie "+port);
            Socket soc= s.accept();
            System.out.println("SERWER>> Klient podlaczony");
            InputStream in= soc.getInputStream();
            OutputStream out= soc.getOutputStream();
            DataInputStream dis=new DataInputStream(in);
            DataOutputStream dos=new DataOutputStream(out);
            Tools.dodajObiekt(new Obiekty("dos",dos,Stan.Serwer));
            Tools.dodajObiekt(new Obiekty("dis",dis,Stan.Serwer));
            ObjectOutputStream oos=new ObjectOutputStream(out);
            Tools.dodajObiekt(new Obiekty("oos",oos,Stan.Serwer));
            ObjectInputStream ois=new ObjectInputStream(in);
            Tools.dodajObiekt(new Obiekty("ois",ois,Stan.Serwer));
            m.wykonaj();

            dis.close();
            dos.close();
            in.close();
            out.close();
            soc.close();
            s.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
