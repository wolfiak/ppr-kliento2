/**
 * Created by pacio on 23.03.2017.
 */
public class ZadanieOdbieranieBrodcast implements Zadanie {
    String nazwa="Odbieranie obiektu brodcast";
    @Override
    public String nazwa() {
        return nazwa;
    }
    @Override
    public void wykonaj() {

       Brodcast b= Tools.czytajObiekt(Stan.Serwer);

       System.out.println("Wiadomosc od: "+b.getOd());
       System.out.println("Widomosc do: "+b.getDokogo());
        System.out.println("Widomosc : "+b.getWiadomosc());
        for (Sasiad s: Sasiad.getSa()) {
            if(s.getNazwa()== b.getOd()){
                s.setNadwaca(true);
            }
        }
    }
}
