/**
 * Created by pacio on 05.04.2017.
 */
public class ZadanieTranzyt implements  Zadanie {
    private Brodcast b;
    private Sasiad s;
    @Override
    public String nazwa() {
        return "Zadanie tranzyt";
    }
    public void obiekt(Brodcast b){
        this.b=b;
    }

    @Override
    public void wykonaj() {
        System.out.println("Klient>> Wysylam obiekt");
        Tools.wyslijObiekt(new Sasiad(Info.Ip,Info.Port,Info.Imie),Stan.Klient);
        System.out.println("Klient>> Obiekt wysalny");
        System.out.println("Klient>> Czekanie na obiekt od serwera");
        s= (Sasiad) Tools.czytajObiektO(Stan.Klient);
        System.out.println("Klient>> Obiekt otrzymany");
        System.out.println("Klient>> Polaczono z "+s.getNazwa());
        System.out.println("KLIENT>> Przekazuje dalej brodcast");
        Tools.wyslijObiekt(b,Stan.Klient);
        System.out.println("KLIENT>> Obiekt przekazany");
    }
}
