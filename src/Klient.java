import java.io.*;
import java.net.Socket;

/**
 * Created by pacio on 08.03.2017.
 */
public class Klient {
    private static String ip;
    private static int port;
    private static MenadzerK m;
    public Klient(String ip, int port,MenadzerK m){
        this.ip=ip;
        this.port=port;
        this.m=m;
    }

    public static void POLACZ(){
        try {
            Socket s=new Socket(ip,port);
            System.out.println("KLIENT>> Klient polaczony z serwerem ("+ip+":"+port+")");
            InputStream in =s.getInputStream();
            OutputStream out= s.getOutputStream();
            DataInputStream dis=new DataInputStream(in);
            DataOutputStream dos=new DataOutputStream(out);
            Tools.dodajObiekt(new Obiekty("dis",dis,Stan.Klient));
            ObjectOutputStream oos=new ObjectOutputStream(out);
            ObjectInputStream ois=new ObjectInputStream(in);
            Tools.dodajObiekt(new Obiekty("ois",ois,Stan.Klient));
            Tools.dodajObiekt(new Obiekty("oos",oos,Stan.Klient));
            m.wykonaj();

            dos.close();
            dis.close();
            out.close();
            in.close();
            s.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
