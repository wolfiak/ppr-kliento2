import javax.xml.crypto.Data;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Scanner;

/**
 * Created by pacio on 08.03.2017.
 */
public class ZadanieSortowanie implements Zadanie {
    String nazwa="Sortowanie";
    @Override
    public String nazwa() {
        return nazwa;
    }
    @Override
    public void wykonaj() {
        DataOutputStream dos=(DataOutputStream) Tools.znajdzObiekt("dos");
        String napiso="";
        System.out.println("Wprowadz ciag zankow");
        Scanner skaner=new Scanner(System.in);
        String napis= skaner.nextLine();
        char[] tab=new char[napis.length()];
        for(int n=0;n< napis.length();n++){
            tab[n]=napis.charAt(n);
        }
        for(int n=0;n<napis.length()/2;n++){
            for(int m=n;m<(napis.length()/2);m++){
                if(tab[n]>tab[m]){
                    char tmp=tab[n];
                    char tmp2=tab[m];
                    tab[n]=tmp2;
                    tab[m]=tmp;
                }
            }

        }

        for(int n=0;n<napis.length();n++){
            napiso+=tab[n];
        }
        System.out.println(napiso);

        Tools.wyslij(dos,napiso);

    }
}
