import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pacio on 08.03.2017.
 */
public  class Tools {
    public static boolean check=true;
    public static Test test;
    private static List<Obiekty> obiekty=new ArrayList<Obiekty>();
    private static Menadzer m=new Menadzer();
    private static MenadzerK mk=new MenadzerK();
    public static boolean decyzja=true;
    private static void dodajZadnanie(Zadanie z){
        System.out.println("Zadanie dodane:");

        m.dodaj(z);
    }
    public static void dodajZadanieK(Zadanie z){mk.dodaj(z);}
    public static void wykonajZadania(){
        m.wykonaj();
    }
    public static void wykonajZadaniaK(){
        mk.wykonaj();
    }
    public static void wyslij(DataOutputStream dos, String napis){
        try {
            System.out.println(">> Wysylam obiekt do klienta");
            byte[] data=napis.getBytes("UTF-8");
            dos.writeInt(data.length);
            dos.write(data);
            System.out.println(">> Obiekt wyslany");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static String czytaj(DataInputStream dis){
        try {
          //  String napis="";
            int dlugosc=dis.readInt();

           System.out.println("Dlugosc odczytana: "+dlugosc);
            byte[] data=new byte[dlugosc];
            dis.readFully(data);

            return new String(data,"UTF-8");


        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";

    }
    public static String czytajAsync(BufferedReader br){
        try {

            //int dlugosc=dis.readInt();

            // System.out.println("Dlugosc odczytana: "+dlugosc);
            //byte[] data=new byte[dlugosc];
            // dis.readFully(data);
            System.out.println(">> Czekam na teks");
            String napis=br.readLine();
            // return new String(data,"UTF-8");
            return napis;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";

    }
    public static void dodajObiekt(Obiekty o){
        obiekty.add(o);
    }
    public static Object znajdzObiekt(String klucz){
        for (int n=0;n<obiekty.size();n++) {
            Obiekty o=obiekty.get(n);
            if(o.getKlucz() == klucz){
              //  System.out.println("current: "+Thread.currentThread().getId()+" obiekt: "+o.getId());
                if(o.getAsync()==true && (Thread.currentThread().getId()==o.getId())) {
                    return o.getWartosc();
                }else if(o.getAsync()==false){
                    return o.getWartosc();
                }
            }
        }
        return "";
    }
    public static Object znajdzObiekt(String klucz,Stan s){
        for (int n=0;n<obiekty.size();n++) {
            Obiekty o=obiekty.get(n);
            if(o.getKlucz() == klucz){
              //  System.out.println("current: "+Thread.currentThread().getId()+" obiekt: "+o.getId());
                if(o.getAsync()==true && (Thread.currentThread().getId()==o.getId())) {
                    return o.getWartosc();
                }else if((o.getAsync()==false) && (o.getStan()==s)){
                    return o.getWartosc();
                }
            }
        }
        return "";
    }
    public static Object znajdzObiekt(String klucz, int id){
        for (Obiekty o: obiekty) {
            if(o.getKlucz() == klucz){
               // System.out.println("current: "+Thread.currentThread().getId()+" obiekt: "+o.getId());
                if(o.getAsync()==true && (id==o.getId())) {
                    return o.getWartosc();
                }else {

                }
            }
        }
        return "";
    }
    public static Object czytajObiektO(Stan s){
        ObjectInputStream ois= (ObjectInputStream) Tools.znajdzObiekt("ois",s);

        try {
            Object br= (Object) ois.readObject();

            return br;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void wyslijObiekt(Brodcast b,Stan s){
        ObjectOutputStream oos= (ObjectOutputStream) Tools.znajdzObiekt("oos",s);

        try {
            //oos.writeObject(new String("dupa"));
            oos.writeObject(b);
            System.err.println("Wyslano obiekt");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public static void wyslijObiekt(Object b,Stan s){
        ObjectOutputStream oos= (ObjectOutputStream) Tools.znajdzObiekt("oos",s);

        try {
            //oos.writeObject(new String("dupa"));
            oos.writeObject(b);
            System.err.println("Wyslano obiekt");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void wyslijObiekty(Brodcast b){
        ObjectOutputStream oos=null;
        for (Sasiad sa:Sasiad.getSa()) {
            if(sa.oos == null){
                System.err.println("Jest null");
            }else{
                System.err.println("Nie jest null");
            }
            oos=sa.oos;
            try {
                oos.writeObject(b);
                oos.flush();
                System.out.println("Wyslalem !");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public static Brodcast czytajObiekt(Stan s){
        ObjectInputStream ois= (ObjectInputStream) Tools.znajdzObiekt("ois",s);

        try {
            Brodcast br= (Brodcast) ois.readObject();

            return br;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void czysc(){
        obiekty.clear();
    }
    public static  void wyswietl(){
        for(int n=0;n<Test.lista.size();n++){
            Test t=Test.lista.get(n);
            System.out.println("Port: "+t.port+" iteracja: "+t.iteracja+" socket: "+t.s.getPort());
        }
    }
}
